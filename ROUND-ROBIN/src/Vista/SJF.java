/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Vista.Ventana;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import SJF.Cola;
import SJF.Proceso;

/**
 *
 * AUTORES ALEJANDOR MORALES, NICOLAS MENESES
 */
public class SJF extends Thread {

    public Cola listos;
    public Cola atendidos;
    public Cola bloqueados;
    private Ventana interfaz;

    public SJF(Cola cola, Ventana interfaz) {
        this.interfaz = interfaz;
        listos = new Cola(interfaz.getTxaConsola());
        this.listos = cola;
    }

    int setEjecucion1=0;
    int setTiempoFinal1 =0;
    int setTiempoRetorno1=0;
    int setTiempoEespera1=0;
    int setRafagaRestante1=0;
    int setTiempoEnCPU1=0;
    int h=0;
    public void run() {

        atendidos = new Cola(interfaz.getTxaConsola());
        bloqueados=new Cola(interfaz.getTxaConsola());
        Proceso enEejecucion;
        Proceso enEejecucion1;
        listos.imprimir(interfaz.getjTableProcesos());
        int y=0;
        int ram1=(int) Math.floor(Math.random()*3+1);
       //JOptionPane.showMessageDialog(null, "proceso que se bloqeuara automaticamente "+ram1);
        while ((listos.colaVacia() == false)) {
            try {
                sleep(250);
                if (listos.colaVacia() == false) {
                	
                    enEejecucion = listos.getCab().getSig();
                  //  if(enEejecucion.getProceso()==ram1 && y==0){
                    //	JOptionPane.showMessageDialog(null, "Se ha bloqueado el proceso "+enEejecucion.getProceso());
                    	//sleep(2000);
                    	//bloqueados.insertar(enEejecucion.getProceso(), enEejecucion.getTiempoInicial(), enEejecucion.getTiempoRafaga(), enEejecucion.getPrioridad());
                    	
                    	//listos.eliminarElemento(enEejecucion, listos);
                    	//y++;
                    	//listos.insertar(enEejecucion.getProceso(), enEejecucion.getTiempoInicial(), enEejecucion.getTiempoRafaga(), enEejecucion.getPrioridad()+40+y);
                    	
                    
                    //}else{
                    	//if(y!=0) {
                    	enEejecucion1 = this.CompararPrioridad(listos.getCab().getSig());
                    	if (enEejecucion1.getRafagaRestante() > 0) {
                    		
                    		if(enEejecucion1.getTiempoRafaga()>4) {
                    			setRafagaRestante1=enEejecucion1.getRafagaRestante();
                    			enEejecucion1.setTiempoRafaga(4);
                    			
                    			interfaz.getProcesos_en_ejecucion().setText(enEejecucion1.getProceso() + "");
                                int f = enEejecucion1.getRafagaRestante();
                                h=f;
                                enEejecucion1.setTiempoEjecucion(Integer.parseInt(interfaz.getTiempo_real().getText()));
                                //enEejecucion.setTiempoEjecucion(Integer.parseInt(interfaz.getTiempo_real().getText()));
                                
                                listos.imprimir(interfaz.getjTableProcesos());
                                for (int i = 1; i <= f; i++) {
                                    sleep(250);
                                    interfaz.getTiempo_real().setText((Integer.parseInt(interfaz.getTiempo_real().getText()) + 1) + "");
                                    enEejecucion1.setTiempoEnCPU(enEejecucion1.getTiempoEnCPU() + 1);
                                    setTiempoEnCPU1=enEejecucion1.getTiempoEnCPU() + 1;
                                  //  interfaz.getTiempo_cpu().setText(enEejecucion.getTiempoEnCPU() + "");
                                    interfaz.getProcesos_en_ejecucion().setText(enEejecucion1.getProceso() + "");
                                    //interfaz.getCpu_nuevo().setText(enEejecucion.getRafagaRestante() + "");
                                    enEejecucion1.setRafagaRestante(enEejecucion1.getRafagaRestante() - 1);
                                   // setRafagaRestante1=(enEejecucion1.getRafagaRestante() - 1);
                                    //JOptionPane.showMessageDialog(null, "jueputa2 "+setRafagaRestante1);
                                    if (this.CompararPrioridad(enEejecucion1).getRafagaRestante() < enEejecucion1.getRafagaRestante()) {
                                        Proceso au1 = listos.buscarProceso(enEejecucion1);
                                        au1 = enEejecucion1;
                                        break;
                                    }

                                    for (int j = 1; j <= enEejecucion1.getTiempoInicial(); j++) {
                                        diagramaGantt(enEejecucion1, false, j);
                                    }

                                    listos.imprimir(interfaz.getjTableProcesos());
                                    diagramaGantt(enEejecucion1, true, Integer.parseInt(interfaz.getTiempo_real().getText()));
                                   
                                    if (enEejecucion1.getTiempoEnCPU() == enEejecucion1.getTiempoRafaga()) {

                                    	 enEejecucion1.setTiempoRafaga(setRafagaRestante1-4);

                                       // interfaz.getTiempo_cpu().setText(enEejecucion.getTiempoEnCPU() + "");
                                        sleep(1000);
                                        atendidos.insertar(enEejecucion1.getProceso(), enEejecucion1.getTiempoInicial(), enEejecucion1.getTiempoRafaga(), enEejecucion1.getPrioridad());
                                        Proceso aux = listos.buscarProceso(enEejecucion1);
                                        int tfinal = Integer.parseInt(interfaz.getTiempo_real().getText());
                                        //int tfinal1 = Integer.parseInt(interfaz.getTiempo_real().getText());
                                        aux.setTiempoFinal(tfinal);
                                        setTiempoFinal1=tfinal;
                                        int tretorno = tfinal - enEejecucion1.getTiempoInicial();
                                        aux.setTiempoRetorno(tretorno);
                                        setTiempoRetorno1=tretorno;
                                        int tespera = tretorno - enEejecucion1.getTiempoRafaga();
                                        if(enEejecucion1.getPrioridad()>=40) {
                                        	aux.setTiempoEespera(tespera+2);
                                        	aux.setPrioridad(enEejecucion1.getPrioridad()-40-y);
                                        }else {
                                        aux.setTiempoEespera(tespera);
                                        }setTiempoEespera1=tespera;
                                        listos.imprimir(interfaz.getjTableProcesos());
                                        listos.eliminarElemento(enEejecucion1, listos);
                                        listos.insertar(enEejecucion1.getProceso(), enEejecucion1.getTiempoInicial(), enEejecucion1.getTiempoRafaga(), enEejecucion1.getPrioridad());
                                    	
                                        sleep(1000);//2000
                                        i = f;
                                     
                                        interfaz.getProcesos_en_ejecucion().setText("");
                                        
                                    }

                                }
                    		}else {
                    			setRafagaRestante1=enEejecucion1.getRafagaRestante();
                                interfaz.getProcesos_en_ejecucion().setText(enEejecucion1.getProceso() + "");
                                int f = enEejecucion1.getRafagaRestante();
                                h=f;
                                enEejecucion1.setTiempoEjecucion(Integer.parseInt(interfaz.getTiempo_real().getText()));
                                //enEejecucion.setTiempoEjecucion(Integer.parseInt(interfaz.getTiempo_real().getText()));
                                
                                listos.imprimir(interfaz.getjTableProcesos());
                                for (int i = 1; i <= f; i++) {
                                    sleep(250);
                                    interfaz.getTiempo_real().setText((Integer.parseInt(interfaz.getTiempo_real().getText()) + 1) + "");
                                    enEejecucion1.setTiempoEnCPU(enEejecucion1.getTiempoEnCPU() + 1);
                                    setTiempoEnCPU1=enEejecucion1.getTiempoEnCPU() + 1;
                                  //  interfaz.getTiempo_cpu().setText(enEejecucion.getTiempoEnCPU() + "");
                                    interfaz.getProcesos_en_ejecucion().setText(enEejecucion1.getProceso() + "");
                                    //interfaz.getCpu_nuevo().setText(enEejecucion.getRafagaRestante() + "");
                                    enEejecucion1.setRafagaRestante(enEejecucion1.getRafagaRestante() - 1);
                                   // setRafagaRestante1=(enEejecucion1.getRafagaRestante() - 1);
                                    //JOptionPane.showMessageDialog(null, "jueputa2 "+setRafagaRestante1);
                                    if (this.CompararPrioridad(enEejecucion1).getRafagaRestante() < enEejecucion1.getRafagaRestante()) {
                                        Proceso au1 = listos.buscarProceso(enEejecucion1);
                                        au1 = enEejecucion1;
                                        break;
                                    }

                                    for (int j = 1; j <= enEejecucion1.getTiempoInicial(); j++) {
                                        diagramaGantt(enEejecucion1, false, j);
                                    }

                                    listos.imprimir(interfaz.getjTableProcesos());
                                    diagramaGantt(enEejecucion1, true, Integer.parseInt(interfaz.getTiempo_real().getText()));

                                    if (enEejecucion1.getTiempoEnCPU() == enEejecucion1.getTiempoRafaga()) {

                                        

                                       // interfaz.getTiempo_cpu().setText(enEejecucion.getTiempoEnCPU() + "");
                                        sleep(1000);
                                        atendidos.insertar(enEejecucion1.getProceso(), enEejecucion1.getTiempoInicial(), enEejecucion1.getTiempoRafaga(), enEejecucion1.getPrioridad());
                                        Proceso aux = listos.buscarProceso(enEejecucion1);
                                        int tfinal = Integer.parseInt(interfaz.getTiempo_real().getText());
                                        //int tfinal1 = Integer.parseInt(interfaz.getTiempo_real().getText());
                                        aux.setTiempoFinal(tfinal);
                                        setTiempoFinal1=tfinal;
                                        int tretorno = tfinal - enEejecucion1.getTiempoInicial();
                                        aux.setTiempoRetorno(tretorno);
                                        setTiempoRetorno1=tretorno;
                                        int tespera = tretorno - enEejecucion1.getTiempoRafaga();
                                        if(enEejecucion1.getPrioridad()>=40) {
                                        	aux.setTiempoEespera(tespera+2);
                                        	aux.setPrioridad(enEejecucion1.getPrioridad()-40-y);
                                        }else {
                                        aux.setTiempoEespera(tespera);
                                        }setTiempoEespera1=tespera;
                                        listos.imprimir(interfaz.getjTableProcesos());
                                        listos.eliminarElemento(enEejecucion1, listos);
                                        //listos.insertar(enEejecucion1.getProceso(), enEejecucion1.getTiempoInicial(), enEejecucion1.getTiempoRafaga(), enEejecucion1.getPrioridad());
                                    	
                                        sleep(1000);//2000
                                        i = f;
                                     
                                        interfaz.getProcesos_en_ejecucion().setText("");
                                        
                                    }
                    		}
                    		

                        }
                    	
                   
                }
               // }
                }

            } catch (InterruptedException ex) {
                Logger.getLogger(SJF.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        JOptionPane.showMessageDialog(null, "Ejecucion Finalizada");

    }

    public Proceso CompararPrioridad(Proceso entrante) {

        Proceso comparado = entrante;
        Proceso aux_inicial = listos.getCab().getSig();
        while (aux_inicial != null) {
            if (entrante.getPrioridad() > aux_inicial.getPrioridad()) {
                comparado = aux_inicial;
            }
            aux_inicial = aux_inicial.getSig();
        }
        return comparado;
    }

    public void diagramaGantt(Proceso actual, boolean estado, int transcurrido) {//actualiza diagrama de gantt desde la interfaz grafica
        String fase;
        if (estado) {
            fase = "X";
        } else {
            fase = "E";
        }
        interfaz.getjTable1().setValueAt(fase, actual.getProceso() - 1, transcurrido);
        interfaz.getjTable1().setDefaultRenderer(Object.class, new Gantt1());
    }

  

    int conteo_tiempo = 0;//variable usada para graficar el primer diagrama de gantt
    String diagramaGant1 = "";//variable usada para graficar el primer diagrama de gantt
    String diagramaGant2 = "0";//variable usada para graficar el segundo diagrama de gantt
}
