
package SJF;

/**
 *
 * AUTORES ALEJANDRO MORALES, NICOL�S MENESES
 */
public class Proceso {
    
    private int proceso;
    private int tiempoCmienzoEjecucion;
 //   private int tiempoCmienzoEjecucion1;
    private int tiempoInicial;
    private int tiempoRafaga;
    private int tiempoFinal;
    private int tiempoRetorno;
    private int tiempoEespera;
    private int tiempoInicial1;
    private int TiempoEnCPU1;
    private int Prioridad1;
    public int getTiempoEnCPU1() {
		return TiempoEnCPU1;
	}

	public void setTiempoEnCPU1(int tiempoEnCPU1) {
		TiempoEnCPU1 = tiempoEnCPU1;
	}

	public int getPrioridad1() {
		return Prioridad1;
	}

	public void setPrioridad1(int prioridad1) {
		Prioridad1 = prioridad1;
	}

	public int getTiempoCmienzoEjecucion1() {
		return tiempoCmienzoEjecucion1;
	}

	public void setTiempoCmienzoEjecucion1(int tiempoCmienzoEjecucion1) {
		this.tiempoCmienzoEjecucion1 = tiempoCmienzoEjecucion1;
	}

	public int getTiempoCmienzoEjecucion() {
		return tiempoCmienzoEjecucion;
	}

	public void setTiempoCmienzoEjecucion(int tiempoCmienzoEjecucion) {
		this.tiempoCmienzoEjecucion = tiempoCmienzoEjecucion;
	}

	public int getTiempoRafaga1() {
		return tiempoRafaga1;
	}

	public void setTiempoRafaga1(int tiempoRafaga1) {
		this.tiempoRafaga1 = tiempoRafaga1;
	}

	public int getTiempoFinal1() {
		return tiempoFinal1;
	}

	public void setTiempoFinal1(int tiempoFinal1) {
		this.tiempoFinal1 = tiempoFinal1;
	}

	public int getTiempoRetorno1() {
		return tiempoRetorno1;
	}

	public void setTiempoRetorno1(int tiempoRetorno1) {
		this.tiempoRetorno1 = tiempoRetorno1;
	}

	public int getTiempoEespera1() {
		return tiempoEespera1;
	}

	public void setTiempoEespera1(int tiempoEespera1) {
		this.tiempoEespera1 = tiempoEespera1;
	}

	private int tiempoRafaga1;
    private int tiempoFinal1;
    private int tiempoRetorno1;
    private int tiempoEespera1;
    private int rafagaRestante;
    private int rafagaRestante1;
    private int tiempoEnCPU;
    
    private int prioridad;
    private Proceso sig;
    private Proceso sig1;
	private int tiempoCmienzoEjecucion1;

    public int getPrioridad() {
        return prioridad;
    }

    public void setPrioridad(int prioridad) {
        this.prioridad = prioridad;
    }

    
   

    public int getTiempoEnCPU() {
        return tiempoEnCPU;
    }

    public void setTiempoEnCPU(int tiempoEnCPU) {
        this.tiempoEnCPU = tiempoEnCPU;
    }

    
    
    public int getRafagaRestante() {
        return rafagaRestante;
    }

    public void setRafagaRestante(int rafagaRestante) {
        this.rafagaRestante = rafagaRestante;
    }
    
    
    
    public int getEjecucion() {
        return tiempoCmienzoEjecucion;
    }

    public void setEjecucion(int ejecucion) {
        this.tiempoCmienzoEjecucion = ejecucion;
    }
    
    public int getProceso() {
        return proceso;
    }

    public void setProceso(int proceso) {
        this.proceso = proceso;
    }

    public int getTiempoInicial() {
        return tiempoInicial;
    }

    public void setTiempoInicial(int tiempoInicial) {
        this.tiempoInicial = tiempoInicial;
    }

    public int getTiempoRafaga() {
        return tiempoRafaga;
    }

    public void setTiempoRafaga(int tiempoRafaga) {
        this.tiempoRafaga = tiempoRafaga;
    }

    public Proceso getSig() {
        return sig;
    }

    public void setSig(Proceso sig) {
        this.sig = sig;
    }

    public int getTiempoEjecucion() {
        return tiempoCmienzoEjecucion;
    }

    public void setTiempoEjecucion(int tiempoEjecucion) {
        this.tiempoCmienzoEjecucion = tiempoEjecucion;
    }

    public int getTiempoFinal() {
        return tiempoFinal;
    }

    public void setTiempoFinal(int tiempoFinal) {
        this.tiempoFinal = tiempoFinal;
    }

    public int getTiempoRetorno() {
        return tiempoRetorno;
    }

    public void setTiempoRetorno(int tiempoRetorno) {
        this.tiempoRetorno = tiempoRetorno;
    }

    public int getTiempoEespera() {
        return tiempoEespera;
    }

    public void setTiempoEespera(int tiempoEespera) {
        this.tiempoEespera = tiempoEespera;
    }

	public int getRafagaRestante1() {
		return rafagaRestante1;
	}

	public void setRafagaRestante1(int rafagaRestante1) {
		this.rafagaRestante1 = rafagaRestante1;
	}

	public int getTiempoInicial1() {
		return tiempoInicial1;
	}

	public void setTiempoInicial1(int tiempoInicial1) {
		this.tiempoInicial1 = tiempoInicial1;
	}

	public int getEjecucion1() {
        return tiempoCmienzoEjecucion1;
    }

    public void setEjecucion1(int ejecucion) {
        this.tiempoCmienzoEjecucion1 = ejecucion;
    }

	public Proceso getSig1() {
		return sig1;
	}

	public void setSig1(Proceso sig1) {
		this.sig1 = sig1;
	}

	
    
    
}
